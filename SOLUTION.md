This is the solution for the "paging-mission-control" programming challenge.

# Assumptions

## Your machine has a Java Development Kit (JDK) installed
This solution is written in Java and uses the Gradle build tool. Gradle will require JDK features to complete the build 
and report generation.

## Your machine is internet-connected
Gradle bundles scripts that retrieve the appropriate version of Gradle so that you can build the solution without needing
to install Gradle directly on your system. The bundled script used in this solution is "gradlew".

## Your machine is Linux-based
This was tested only on Linux. Note that there is a script, "gradlew.bat", that is intended for use on Windows. Feel free
to try it out!

## Your machine has Firefox installed
Firefox is used for viewing code reports. If Firefox isn't installed, then you can still view the reports; just use your 
favorite browser and point it to the report locations given below.

# Building and Gathering Reports
To build the project and gather reports, change into the project and execute the following:
```bash
./gradlew clean build javadoc jacocoTestReport
```

To view the reports, execute the following (after the above command has been executed):
```bash
firefox build/docs/javadoc/index.html build/reports/tests/test/index.html build/reports/jacoco/test/html/index.html build/reports/spotbugs/main.html build/reports/checkstyle/main.html
```
The reports viewed are:
* JavaDocs
* Test results
* Code coverage
* Static analysis
* Style checking

# Running the Application
The following will execute the application with the example input given by the challenge.
```bash
./gradlew run
```

What's the fun in that? We want to pass **any** input to the application. To do this, do the following:
```bash
./gradlew run --args="<path-to-input-file>"
```
OR do this after executing the build steps above:
```bash
java -jar build/libs/paging-mission-control.jar <path-to-input-file>
```

