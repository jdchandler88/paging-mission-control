package paging.mission.control;

import org.apache.commons.io.FilenameUtils;
import paging.mission.control.alert.AlertCollector;
import paging.mission.control.alert.AlertGenerator;
import paging.mission.control.alert.AlertPrettyPrinter;
import paging.mission.control.telemetry.SystemId;
import paging.mission.control.telemetry.TelemetryStatus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * This is the main application. It reads from a file and gives telemetry data to the monitoring
 * system. Upon instantiation of this class, the file will be read. Alerts can be gathered from
 * the instance after creation, if any were generated
 */
public class App {

    private String telemetryDataFilename;

    private AlertCollector alertCollector = new AlertCollector();

    private AlertPrettyPrinter alertPrettyPrinter = new AlertPrettyPrinter();

    private Map<SystemId, AlertGenerator> alertGeneratorBySystemId = new HashMap<>();

    /**
     * Creates instance of App. Telemetry data is instantly submitted to alert generators.
     * @param telemetryDataFilename file from which telemetry data is read
     */
    public App(String telemetryDataFilename) {
        this.telemetryDataFilename = telemetryDataFilename;
        passTelemetryFromFileToMonitoringSystem();
    }

    public String getAlertsAsString() {
        return alertPrettyPrinter.getAllAlertsAsString(this.alertCollector.getAlerts());
    }

    public void printAlerts() {
        alertPrettyPrinter.printAllAlerts(this.alertCollector.getAlerts());
    }

    private void passTelemetryFromFileToMonitoringSystem() {
        //open file pointed to by first arg
        try (BufferedReader reader = new BufferedReader(
                new FileReader(
                        new File(
                                FilenameUtils.getName(this.telemetryDataFilename)),
                                StandardCharsets.UTF_8))
        ) {
            String rawTelemetryDatapoint = null;
            //parse lines; each line is a point of telemetry status
            while ((rawTelemetryDatapoint = reader.readLine()) != null)  {
                TelemetryStatus telemetryStatus = parseTelemetryStatus(rawTelemetryDatapoint);
                createAlertGeneratorForSystemIfNotExists(telemetryStatus.getSystemId());
                submitTelemetryStatusToAlertGenerator(telemetryStatus);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private TelemetryStatus parseTelemetryStatus(String rawTelemetryData) {
        return TelemetryStatus.fromString(rawTelemetryData);
    }

    private void createAlertGeneratorForSystemIfNotExists(SystemId systemId) {
        if (!alertGeneratorBySystemId.containsKey(systemId)) {
            alertGeneratorBySystemId.put(systemId, new AlertGenerator(alertCollector));
        }
    }

    private void submitTelemetryStatusToAlertGenerator(TelemetryStatus telemetryStatus) {
        AlertGenerator alertGenerator = alertGeneratorBySystemId.get(telemetryStatus.getSystemId());
        alertGenerator.submitTelemetryStatus(telemetryStatus);
    }

    /**
     * Entry point for the telemetry monitoring application. This invokes the application object.
     * @param args input arguments
     */
    public static void main(String[] args) {
        String filename = getFilenameFromArgsOrDefault(args);
        App app = new App(filename);
        app.printAlerts();
    }

    private static String getFilenameFromArgsOrDefault(String[] args) {
        String filename = null;
        if (isValidInput(args)) {
            filename = getFilenameFromArguments(args);
        } else {
            filename = getDefaultFilename();
        }
        return filename;
    }

    private static boolean isValidInput(String[] args) {
        if (args.length == 0) {
            System.out.println("No file provided. Defaulting to example input...");
            return false;
        }
        if (!Files.exists(Paths.get(FilenameUtils.getName(args[0])))) {
            System.out.println(args[0] + " is not a valid file. Defaulting to example input...");
            return false;
        }
        return true;
    }

    private static String getFilenameFromArguments(String[] args) {
        return args[0];
    }

    private static String getDefaultFilename() {
        return "input.txt";
    }


}
