package paging.mission.control.alert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class responsible for collecting alerts as they arrive.
 */
public class AlertCollector {

    private List<Alert> alerts = new ArrayList<>();

    public void addAlert(Alert alert) {
        this.alerts.add(alert);
    }

    /**
     * Gets unmodifiable copy of collected alerts.
     */
    public List<Alert> getAlerts() {
        return Collections.unmodifiableList(alerts);
    }

}
