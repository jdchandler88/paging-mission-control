package paging.mission.control.alert;

/**
 * Class containing information about an alert. These are created by {@link AlertGenerator}
 */
public class Alert {

    private final int satelliteId;

    private final String severity;

    private final String component;

    private final String timestamp;

    /**
     * Creates an alert with the given data.
     * @param satelliteId id of the satellite
     * @param severity severity of the alert
     * @param component component being measured in telemetry
     * @param timestamp timestamp of *first occurrence* of alert condition
     */
    public Alert(int satelliteId, String severity, String component, String timestamp) {
        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component = component;
        this.timestamp = timestamp;
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public String getSeverity() {
        return severity;
    }

    public String getComponent() {
        return component;
    }

    public String getTimestamp() {
        return timestamp;
    }

}
