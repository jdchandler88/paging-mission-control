package paging.mission.control.alert;

import paging.mission.control.Utils;
import paging.mission.control.telemetry.TelemetryStatus;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.Temporal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class detects alert conditions. If conditions are met, then the alert collector is given
 * an alert.
 */
public class AlertGenerator {

    public static final int COUNT_THRESHOLD = 3;

    public static final int TIME_THRESHOLD_MINUTES = 5;

    private final AlertCollector alertCollector;

    private final List<TelemetryStatus> statuses = new ArrayList<>(COUNT_THRESHOLD);

    public AlertGenerator(AlertCollector alertCollector) {
        this.alertCollector = alertCollector;
    }

    /**
     * Submit a telemetry status to this alert generator. If an alert condition is detected, then
     * the AlertGenerator's AlertCollector will be notified.
     * @param telemetryStatus status to submit to monitoring system
     */
    public void submitTelemetryStatus(TelemetryStatus telemetryStatus) {
        statuses.add(telemetryStatus);
        removeAllStatusesOlderThanTimeoutPeriod(
                Utils.getInstantFromInputTimestamp(telemetryStatus.getTimestamp())
        );
        if (isPossibleToGenerateAlert()) {
            checkForAlertConditions(telemetryStatus);
        }
    }

    /**
     * No need in keeping datapoints around that cannot trip an alarm. Clean up storage.
     * @param latestTime time of receipt of latest telemetry status
     */
    private void removeAllStatusesOlderThanTimeoutPeriod(Temporal latestTime) {
        List<TelemetryStatus> expiredStatuses = statuses.stream()
                .filter(status -> isTimeBetweenGreaterThanTimeThreshold(status, latestTime))
                .collect(Collectors.toList());
        statuses.removeAll(expiredStatuses);
    }

    private boolean isTimeBetweenGreaterThanTimeThreshold(TelemetryStatus status,
                                                          Temporal latestTime) {
        Temporal statusInstant = Utils.getInstantFromInputTimestamp(status.getTimestamp());
        return Duration.between(statusInstant, latestTime).toMinutes() > TIME_THRESHOLD_MINUTES;
    }

    /**
     * If there aren't at least COUNT_THESHOLD datapoints, then it is not possible to trigger an
     * alarm.
     * @return true if it's possible to generate an alert. false otherwise.
     */
    private boolean isPossibleToGenerateAlert() {
        return statuses.size() >= COUNT_THRESHOLD;
    }

    private void checkForAlertConditions(TelemetryStatus telemetryStatus) {
        if (generateAlertIfAllMeasurementsExceedThreshold(
                telemetryStatus,
                telemetryStatus.getThresholds().getRedHighLimit(),
                "RED HIGH")) {
            return;
        }
        if (generateAlertIfAllMeasurementsFallBelowThreshold(
                telemetryStatus,
                telemetryStatus.getThresholds().getRedLowLimit(),
                "RED LOW")) {
            return;
        }
    }

    private boolean generateAlertIfAllMeasurementsExceedThreshold(
            TelemetryStatus telemetryStatus, double threshold, String severity
    ) {
        if (statuses.stream()
                .filter(status -> status.getRawValue() > threshold)
                .count() >= COUNT_THRESHOLD
        ) {
            Alert alert = new Alert(
                    telemetryStatus.getSystemId().getSatelliteId(),
                    severity, telemetryStatus.getSystemId().getComponent(),
                    getAlertTimestampForOverage(threshold));
            this.alertCollector.addAlert(alert);
            return true;
        }
        return false;
    }

    private boolean generateAlertIfAllMeasurementsFallBelowThreshold(
            TelemetryStatus telemetryStatus, double threshold, String severity
    ) {
        if (statuses.stream()
                .filter(status -> status.getRawValue() < threshold)
                .count() >= COUNT_THRESHOLD
        ) {
            Alert alert = new Alert(
                    telemetryStatus.getSystemId().getSatelliteId(),
                    severity,
                    telemetryStatus.getSystemId().getComponent(),
                    getAlertTimestampForUnderage(threshold));
            this.alertCollector.addAlert(alert);
            return true;
        }
        return false;
    }

    /**
     * This gets the time at which the *first overage measurement* occurred within the 5 minute
     * window. Note: Would a monitoring system do this? Wouldn't it show the timestamp for the
     * measurement that tripped the alarm? Maybe it should show both instead (time first
     * occurrence, time last occurrence).
     * @return output-formatted timestamp
     */
    private String getAlertTimestampForOverage(double threshold) {
        Instant alertInstant = Utils.getInstantFromInputTimestamp(
                statuses
                        .stream()
                        .filter(status -> status.getRawValue() > threshold)
                        .findFirst()
                        .get().getTimestamp()
        );
        return Utils.getOutputTimestampFormatFromInstant(alertInstant);
    }

    /**
     * This gets the time at which the *first overage measurement* occurred within the 5 minute
     * window. Note: Would a monitoring system do this? Wouldn't it show the timestamp for the
     * measurement that tripped the alarm? Maybe it should show both instead
     * (time first occurrence, time last occurrence).
     * @return output-formatted timestamp
     */
    private String getAlertTimestampForUnderage(double threshold) {
        Instant alertInstant = Utils.getInstantFromInputTimestamp(
                statuses
                        .stream()
                        .filter(status -> status.getRawValue() < threshold)
                        .findFirst()
                        .get().getTimestamp()
        );
        return Utils.getOutputTimestampFormatFromInstant(alertInstant);
    }

}
