package paging.mission.control.alert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

/**
 * Class formats alert objects into JSON strings.
 */
public class AlertPrettyPrinter {

    //JACKSON object responsible for mapping fields to JSON object
    private final ObjectMapper objectMapper;

    /**
     * Creates alert pretty printer. This configures the underlying printing objects.
     */
    public AlertPrettyPrinter() {
        DefaultPrettyPrinter prettyPrinter = new DefaultPrettyPrinter();
        DefaultIndenter indenter = new DefaultIndenter("    ", "\n");
        prettyPrinter.indentArraysWith(indenter);
        prettyPrinter.indentObjectsWith(indenter);
        this.objectMapper = new ObjectMapper();
        this.objectMapper.setDefaultPrettyPrinter(prettyPrinter);
    }

    /**
     * All alerts will be printed as JSON objects into a JSON array.
     * @param allAlerts alerts to convert
     * @return stringified alerts
     */
    public String getAllAlertsAsString(List<Alert> allAlerts) {
        try {
            return this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(allAlerts);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Prints all alerts to console.
     * @param allAlerts alerts to print
     */
    public void printAllAlerts(List<Alert> allAlerts) {
        System.out.println(getAllAlertsAsString(allAlerts));
    }

}
