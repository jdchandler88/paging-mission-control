package paging.mission.control.telemetry;

/**
 * Class representing data measurement of a satellite system.
 */
public class TelemetryStatus {

    //escaped pipe character
    private static final String RAW_FIELD_DELIMITER = "\\|";

    //index used to parse telemetry data. starts at 0
    enum RawStatusFieldsIndex {
        TIMESTAMP,
        SATELLITE_ID,
        RED_HIGH_LIMIT,
        YELLOW_HIGH_LIMIT,
        YELLOW_LOW_LIMIT,
        RED_LOW_LIMIT,
        RAW_VALUE,
        COMPONENT
    }

    private final SystemId systemId;

    private final Thresholds thresholds;

    private final String timestamp;

    private final double rawValue;

    private TelemetryStatus(
            SystemId systemId, Thresholds thresholds, String timestamp, double rawValue
    ) {
        this.systemId = systemId;
        this.thresholds = thresholds;
        this.timestamp = timestamp;
        this.rawValue = rawValue;
    }

    /**
     * Creates an instance of this class from raw telemetry status data.
     * @param rawTelemetryStatus raw data from satellite
     * @return parsed instance of this class
     */
    public static TelemetryStatus fromString(String rawTelemetryStatus) {
        String[] telemetryStatusFields = rawTelemetryStatus.split(RAW_FIELD_DELIMITER);
        SystemId systemId = new SystemId(
                getFieldAsInteger(telemetryStatusFields, RawStatusFieldsIndex.SATELLITE_ID),
                getFieldAsString(telemetryStatusFields, RawStatusFieldsIndex.COMPONENT)
        );
        Thresholds thresholds = new Thresholds(
                getFieldAsDouble(telemetryStatusFields, RawStatusFieldsIndex.RED_HIGH_LIMIT),
                getFieldAsDouble(telemetryStatusFields, RawStatusFieldsIndex.YELLOW_HIGH_LIMIT),
                getFieldAsDouble(telemetryStatusFields, RawStatusFieldsIndex.YELLOW_LOW_LIMIT),
                getFieldAsDouble(telemetryStatusFields, RawStatusFieldsIndex.RED_LOW_LIMIT)
        );
        return new TelemetryStatus(
                systemId,
                thresholds,
                getFieldAsString(telemetryStatusFields, RawStatusFieldsIndex.TIMESTAMP),
                getFieldAsDouble(telemetryStatusFields, RawStatusFieldsIndex.RAW_VALUE)
        );
    }

    public SystemId getSystemId() {
        return systemId;
    }

    public Thresholds getThresholds() {
        return thresholds;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public double getRawValue() {
        return rawValue;
    }

    private static String getFieldAsString(
            String[] telemetryStatusFields, RawStatusFieldsIndex fieldIndex
    ) {
        return telemetryStatusFields[fieldIndex.ordinal()];
    }

    private static int getFieldAsInteger(
            String[] telemetryStatusFields, RawStatusFieldsIndex fieldIndex
    ) {
        return Integer.parseInt(getFieldAsString(telemetryStatusFields, fieldIndex));
    }

    private static double getFieldAsDouble(
            String[] telemetryStatusFields, RawStatusFieldsIndex fieldIndex
    ) {
        return Double.parseDouble(getFieldAsString(telemetryStatusFields, fieldIndex));
    }

}
