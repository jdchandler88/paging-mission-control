package paging.mission.control.telemetry;

import java.util.Objects;

/**
 * Class used to identify a system.
 */
public class SystemId {

    private int satelliteId;

    private String component;

    SystemId(int satelliteId, String component) {
        this.satelliteId = satelliteId;
        this.component = component;
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public String getComponent() {
        return component;
    }

    /**
     * Equality check uses all fields in this class.
     * @param other object to compare
     * @return true if equal, false otherwise
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        SystemId systemId = (SystemId) other;
        return satelliteId == systemId.satelliteId
                && Objects.equals(component, systemId.component);
    }

    /**
     * Every field is used in generating this object's hash code.
     * @return this object's hash code
     */
    @Override
    public int hashCode() {
        return Objects.hash(satelliteId, component);
    }

}
