package paging.mission.control.telemetry;

import paging.mission.control.Utils;

import java.util.Objects;

/**
 * Class containing various thresholds for telemetry measurements to trigger alarms.
 */
public class Thresholds {

    private final double redHighLimit;

    private final double yellowHighLimit;

    private final double yellowLowLimit;

    private final double redLowLimit;

    Thresholds(
            double redHighLimit, double yellowHighLimit, double yellowLowLimit, double redLowLimit
    ) {
        this.redHighLimit = redHighLimit;
        this.yellowHighLimit = yellowHighLimit;
        this.yellowLowLimit = yellowLowLimit;
        this.redLowLimit = redLowLimit;
    }

    public double getRedHighLimit() {
        return redHighLimit;
    }

    public double getYellowHighLimit() {
        return yellowHighLimit;
    }

    public double getYellowLowLimit() {
        return yellowLowLimit;
    }

    public double getRedLowLimit() {
        return redLowLimit;
    }

    /**
     * Every field in this object is used for equality check. The values are compared using
     * {@link Utils#doubleEquals(double, double)}.
     * @param other object to compare
     * @return true if equal, false otherwise
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Thresholds that = (Thresholds) other;
        return Utils.doubleEquals(that.redHighLimit, redHighLimit)
                && Utils.doubleEquals(that.yellowHighLimit, yellowHighLimit)
                && Utils.doubleEquals(that.yellowLowLimit, yellowLowLimit)
                && Utils.doubleEquals(that.redLowLimit, redLowLimit);
    }

    /**
     * Every field in this object is used for generating hash code.
     * @return this object's hash code
     */
    @Override
    public int hashCode() {
        return Objects.hash(redHighLimit, yellowHighLimit, yellowLowLimit, redLowLimit);
    }

}
