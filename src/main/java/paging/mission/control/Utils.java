package paging.mission.control;

import org.apache.commons.io.FilenameUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

/**
 * Utility methods for use throughout the application.
 */
public class Utils {

    public static final double DOUBLE_COMPARE_THRESHOLD = .0001;

    /**
     * Compares two doubles. Uses {@link Utils#DOUBLE_COMPARE_THRESHOLD} as the threshold
     * @param first first double
     * @param second second double
     * @return true if mostly equal (within threshold). false otherwise
     */
    public static boolean doubleEquals(double first, double second) {
        return Math.abs(first - second) <= DOUBLE_COMPARE_THRESHOLD;
    }

    /**
     * Parses an Instant using the format given by requirements.
     * @param timestamp timestamp from telemetry
     * @return parsed Instant
     */
    public static Instant getInstantFromInputTimestamp(String timestamp) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
        try {
            Date date = sdf.parse(timestamp);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            return cal.toInstant();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Converts Instant to output timestamp format given by requirements.
     * @param instant instant to convert
     * @return converted instant
     */
    public static String getOutputTimestampFormatFromInstant(Instant instant) {
        //2018-01-01T23:01:38.001Z
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        return sdf.format(Date.from(instant));
    }

}
