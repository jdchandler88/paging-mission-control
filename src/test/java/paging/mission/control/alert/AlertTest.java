package paging.mission.control.alert;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AlertTest {

    private Alert cut;

    private static final int SATELLITE_ID = 1;

    private static final String COMPONENT = "component";

    private static final String SEVERITY = "severity";

    private static final String TIMESTAMP = "timestamp";

    @BeforeEach
    void init() {
        cut = new Alert(SATELLITE_ID, SEVERITY, COMPONENT, TIMESTAMP);
    }

    @Test
    void constructorStoresDataAppropriately() {
        assertAll(
            () -> assertEquals(SATELLITE_ID, cut.getSatelliteId()),
            () -> assertEquals(COMPONENT, cut.getComponent()),
            () -> assertEquals(SEVERITY, cut.getSeverity()),
            () -> assertEquals(TIMESTAMP, cut.getTimestamp())
        );
    }

}