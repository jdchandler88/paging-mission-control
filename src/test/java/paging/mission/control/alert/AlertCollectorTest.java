package paging.mission.control.alert;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class AlertCollectorTest {

    @Test
    void addAlertResultsInSize1AlertList() {
        AlertCollector cut = new AlertCollector();
        cut.addAlert(new Alert(1, "", "", ""));
        assertEquals(1, cut.getAlerts().size());
    }

    @Test
    void attemptingToModifyAlertsCollectionThrowsException() {
        assertThrows(Exception.class,
            () -> new AlertCollector().getAlerts().add(new Alert(1, "", "", "")));
    }

}