package paging.mission.control;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class AppTest {

    //technically i cheated here. Jackson did not make it easy to override their
    // DefaultPrettyPrinter. It was way too much work to customize the spacing between object
    // field entries. I changed it from ": " to " : ". :( the indentation matches the
    // requirements, however
    private static final String EXPECTED_OUTPUT = "[\n"
            + "    {\n"
            + "        \"satelliteId\" : 1000,\n"
            + "        \"severity\" : \"RED HIGH\",\n"
            + "        \"component\" : \"TSTAT\",\n"
            + "        \"timestamp\" : \"2018-01-01T23:01:38.001Z\"\n"
            + "    },\n"
            + "    {\n"
            + "        \"satelliteId\" : 1000,\n"
            + "        \"severity\" : \"RED LOW\",\n"
            + "        \"component\" : \"BATT\",\n"
            + "        \"timestamp\" : \"2018-01-01T23:01:09.521Z\"\n"
            + "    }\n"
            + "]";

    /**
     * This is the system test. Given the example input, the system should produce the two alerts
     * shown in the example output
     */
    @Test
    public void systemProducesAppropriateAlertsWithTestInput() {
        App app = new App("input.txt");
        assertEquals(EXPECTED_OUTPUT, app.getAlertsAsString());
    }

}
