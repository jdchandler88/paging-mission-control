package paging.mission.control.telemetry;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

class SystemIdTest {

    @Test
    void equalsWhenFieldsSame() {
        SystemId first = new SystemId(1, "comp");
        SystemId second = new SystemId(1, "comp");
        assertEquals(first, second);
    }

    @Test
    void hashEqualsWhenObjectsEqual() {
        SystemId first = new SystemId(1, "comp");
        SystemId second = new SystemId(1, "comp");
        assertAll(
            () -> assertEquals(first, second),
            () -> assertEquals(first.hashCode(), second.hashCode())
        );
    }

    @Test
    void notEqualWhenIdDifferent() {
        SystemId first = new SystemId(1, "comp");
        SystemId second = new SystemId(2, "comp");
        assertNotEquals(first, second);
    }

    @Test
    void notEqualWhenComponentDifferent() {
        SystemId first = new SystemId(1, "comp");
        SystemId second = new SystemId(1, "differentComp");
        assertNotEquals(first, second);
    }

}