package paging.mission.control.telemetry;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class TelemetryStatusTest {

    //<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
    @ParameterizedTest
    @CsvSource(value = {
            "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT,  20180101 23:01:05.001, 1001, 101, 98, 25, 20, 99.9,  TSTAT",
            "20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT,       20180101 23:01:09.521, 1000, 17,  15, 9,  8,  7.8,   BATT",
            "20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT,  20180101 23:01:26.011, 1001, 101, 98, 25, 20, 99.8,  TSTAT",
            "20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT, 20180101 23:01:38.001, 1000, 101, 98, 25, 20, 102.9, TSTAT",
            "20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT,  20180101 23:01:49.021, 1000, 101, 98, 25, 20, 87.9,  TSTAT",
            "20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT,  20180101 23:02:09.014, 1001, 101, 98, 25, 20, 89.3,  TSTAT",
            "20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT,  20180101 23:02:10.021, 1001, 101, 98, 25, 20, 89.4,  TSTAT",
            "20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT,       20180101 23:02:11.302, 1000, 17,  15, 9,  8,  7.7,   BATT",
            "20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT, 20180101 23:03:03.008, 1000, 101, 98, 25, 20, 102.7, TSTAT",
            "20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT, 20180101 23:03:05.009, 1000, 101, 98, 25, 20, 101.2, TSTAT",
            "20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT,  20180101 23:04:06.017, 1001, 101,  98, 25, 20, 89.9,  TSTAT",
            "20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT,       20180101 23:04:11.531, 1000, 17,  15, 9,  8,  7.9,   BATT",
            "20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT,  20180101 23:05:05.021, 1001, 101, 98, 25, 20, 89.9,  TSTAT",
            "20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT,       20180101 23:05:07.421, 1001, 17,  15, 9,  8,  7.9,   BATT"
    })
    void parsesInputDataCorrectly(
        String rawTelemetryStatus,
        String timestamp,
        int satelliteId,
        double redHighLimit,
        double yellowHighLimit,
        double yellowLowLimit,
        double redLowLimit,
        double rawValue,
        String component
    ) {
        TelemetryStatus parsedStatus = TelemetryStatus.fromString(rawTelemetryStatus);
        assertAll("Asserting that example telemetry data is parsed properly.",
            () -> assertEquals(satelliteId, parsedStatus.getSystemId().getSatelliteId()),
            () -> assertEquals(component, parsedStatus.getSystemId().getComponent()),
            () -> assertEquals(timestamp, parsedStatus.getTimestamp()),
            () -> assertEquals(redHighLimit, parsedStatus.getThresholds().getRedHighLimit()),
            () -> assertEquals(yellowHighLimit, parsedStatus.getThresholds().getYellowHighLimit()),
            () -> assertEquals(yellowLowLimit, parsedStatus.getThresholds().getYellowLowLimit()),
            () -> assertEquals(redLowLimit, parsedStatus.getThresholds().getRedLowLimit()),
            () -> assertEquals(rawValue, parsedStatus.getRawValue())
        );
    }

}