package paging.mission.control.telemetry;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

class ThresholdsTest {

    @Test
    void equalsWhenAllFieldsAreTheSame() {
        double redLow = 1;
        double redHigh = 2;
        double yellowLow = 1;
        double yellowHigh = 2;
        Thresholds a1 = new Thresholds(redHigh, yellowHigh, yellowLow, redLow);
        Thresholds a2 = new Thresholds(redHigh, yellowHigh, yellowLow, redLow);
        assertEquals(a1, a2);
    }

    @Test
    void notEqualWhenRedLowDifferent() {
        double redLow = 1;
        double redHigh = 2;
        double yellowLow = 1;
        double yellowHigh = 2;
        Thresholds a1 = new Thresholds(redHigh, yellowHigh, yellowLow, redLow);
        Thresholds a2 = new Thresholds(redHigh, yellowHigh, yellowLow, 0);
        assertNotEquals(a1, a2);
    }

    @Test
    void notEqualWhenRedHighDifferent() {
        double redLow = 1;
        double redHigh = 2;
        double yellowLow = 1;
        double yellowHigh = 2;
        Thresholds a1 = new Thresholds(redHigh, yellowHigh, yellowLow, redLow);
        Thresholds a2 = new Thresholds(3, yellowHigh, yellowLow, redLow);
        assertNotEquals(a1, a2);
    }

    @Test
    void notEqualWhenYellowLowDifferent() {
        double redLow = 1;
        double redHigh = 2;
        double yellowLow = 1;
        double yellowHigh = 2;
        Thresholds a1 = new Thresholds(redHigh, yellowHigh, yellowLow, redLow);
        Thresholds a2 = new Thresholds(redHigh, yellowHigh, 0, redLow);
        assertNotEquals(a1, a2);
    }

    @Test
    void notEqualWhenYellowHighDifferent() {
        double redLow = 1;
        double redHigh = 2;
        double yellowLow = 1;
        double yellowHigh = 2;
        Thresholds a1 = new Thresholds(redHigh, yellowHigh, yellowLow, redLow);
        Thresholds a2 = new Thresholds(redHigh, 3, yellowLow, redLow);
        assertNotEquals(a1, a2);
    }

    @Test
    void hashIsSameWhenEqual() {
        double redLow = 1;
        double redHigh = 2;
        double yellowLow = 1;
        double yellowHigh = 2;
        Thresholds a1 = new Thresholds(redHigh, yellowHigh, yellowLow, redLow);
        Thresholds a2 = new Thresholds(redHigh, yellowHigh, yellowLow, redLow);
        assertAll(
            () -> assertEquals(a1, a2),
            () -> assertEquals(a1.hashCode(), a2.hashCode())
        );

    }

}