package paging.mission.control;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class UtilsTest {

    @Test
    void doubleEqualsWithIdenticalInputs() {
        double input = 1.00001;
        assertTrue(Utils.doubleEquals(input, input));
    }

    @Test
    void doubleEqualsWithDifferentInputsWithinThreshold() {
        double thresholdDividedBy2 = Utils.DOUBLE_COMPARE_THRESHOLD / 2;
        double first = 1.0001;
        double second = first + thresholdDividedBy2;
        assertTrue(Utils.doubleEquals(first, second));
    }

    @Test
    void doubleEqualsWhenDifferenceEqualsThreshold() {
        double first = 1 + Utils.DOUBLE_COMPARE_THRESHOLD;
        double second = first + Utils.DOUBLE_COMPARE_THRESHOLD;
        assertTrue(Utils.doubleEquals(first, second));
    }

    @Test
    void doubleDoesNotEqualWhenGreaterThanThreshold() {
        double thresholdMultipliedBy2 = Utils.DOUBLE_COMPARE_THRESHOLD * 2;
        double first = 1.0001;
        double second = first + thresholdMultipliedBy2;
        assertFalse(Utils.doubleEquals(first, second));
    }

}